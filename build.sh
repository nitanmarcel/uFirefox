#!/bin/bash
pkgs=("$@")
for pkg in "${pkgs[@]}"
do
	if [[ "${pkg}" == *":all" ]]; then
		dpkgs+="${pkg} "
	else
		dpkgs+="${pkg}":"${ARCH} "
	fi
done
apt-get -o Dir::Cache=${BUILD_DIR} -qq update
apt-get -o Dir::Cache=${BUILD_DIR} install -y --download-only ${dpkgs} || exit 1

while read package
do
	dpkg-deb -x "${package}" ${BUILD_DIR}
done < <(ls ${BUILD_DIR}/archives/*.deb);

[ -d ${BUILD_DIR}/lib ] && cp -r ${BUILD_DIR}/lib/* ${CLICK_LD_LIBRARY_PATH} || exit 2
[ -d ${BUILD_DIR}/usr/lib ] && cp -r ${BUILD_DIR}/usr/lib/* ${CLICK_LD_LIBRARY_PATH}
[ -d ${CLICK_LD_LIBRARY_PATH}/${ARCH_TRIPLET} ] && { cp ${CLICK_LD_LIBRARY_PATH}/${ARCH_TRIPLET}/* ${CLICK_LD_LIBRARY_PATH};rm -rf ${CLICK_LD_LIBRARY_PATH}/${ARCH_TRIPLET}; }
[ -d ${BUILD_DIR}/usr/bin ] && cp -r ${BUILD_DIR}/usr/bin/* ${CLICK_PATH} || exit 2
[ -d ${BUILD_DIR}/usr/sbin ] && cp -r ${BUILD_DIR}/usr/sbin/* ${CLICK_PATH}
[ -d ${BUILD_DIR}/usr/share ] && cp -r ${BUILD_DIR}/usr/share ${INSTALL_DIR}
[ -d ${BUILD_DIR}/usr/games ] && cp -r ${BUILD_DIR}/usr/games/* ${CLICK_PATH}
bash ./adapt.sh
tree install
pwd
